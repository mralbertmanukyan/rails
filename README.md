# Rails stand-alone and Dockerized

We have 2 solutions here one of them is Dockerizing another is stand-alone 

# Dockerized

DB configuration needs to be done

## Repo details

Dockerizing solution is done by gitlab ci, any commit before I have moved .gitlab-ci.yml file to dicrectory woldeve trigger a pipline which will run the CI section by building and pushing container to docker hub.

you can find .gitlab-ci.yml in [Dockerizing](https://gitlab.com/mralbertmanukyan/rails/-/blob/dev/Dockerizing/.gitlab-ci.yml) directory

Secrets for container registry are in gitlab > repo > settings > CI/CD > Variables section which are not going to be visible for you

## Docker
```bash
# Docker engine is required
docker pull techalik1994/rails:latest
```

To run a container use the following command
```bash
docker run --rm -p 3000:3000 techalik1994/rails:latest
```
this will run a container and will show on your screen `Hi P161` message

```bash
docker run --rm -p 3000:3000 techalik1994/rails:latest 'any comment here'
```
this will run a container and will show on your screen message you have passed to it

# Stand-alone

DB configuration needs to be done

```bash
`pwd`start.sh
```
this will run an application and will show on your screen `Hi P161` message

```bash
`pwd`start.sh 'any comment here'
```
this will run a container and will show on your screen message you have passed to it

# Refears

1. [Getting Started with Rails](https://guides.rubyonrails.org/getting_started.html)
2. [Ruby on Rails 5 Hello World Example](https://www.javatpoint.com/ruby-on-rails-hello-world-example)
3. [webpacker configuration file not found webpacker.yml](https://errorsandfixes.bookmark.com/webpacker-configuration-file-not-found-webpacker-yml)
4. ...
