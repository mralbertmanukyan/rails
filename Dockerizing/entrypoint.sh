#!/bin/bash
cd /usr/src/app/hello_world 
rails generate controller hello index --skip-routes
echo "
Rails.application.routes.draw do
  root 'hello#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
" > /usr/src/app/hello_world/config/routes.rb
if [ -z $1 ]; then
	echo "<h1>Hi P161</h1>" > /usr/src/app/hello_world/app/views/hello/index.html.erb
else
	echo "<h1>${1}</h1>" > /usr/src/app/hello_world/app/views/hello/index.html.erb
fi

rails server -b 0.0.0.0
